---
wsId: 
title: Mycelium Testnet Wallet
altTitle: 
authors:
- leo
users: 10000
appId: com.mycelium.testnetwallet
appCountry: 
released: 2013-10-04
updated: 2022-12-29
version: 3.16.0.15-TESTNET
stars: 4.2
ratings: 146
reviews: 10
size: 
website: https://mycelium.com
repository: 
issue: 
icon: com.mycelium.testnetwallet.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-08-02
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 

---

This is the testnet version of {% include walletLink.html wallet='android/com.mycelium.wallet' verdict='true' %}