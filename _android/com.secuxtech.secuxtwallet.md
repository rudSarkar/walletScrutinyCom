---
wsId: 
title: SecuX Wallet for SecuX Nifty Hardware Wallet
altTitle: 
authors: 
- danny
users: 500
appId: com.secuxtech.secuxtwallet
appCountry: TW
released: 2022-05-20
updated: 2022-10-07
version: 1.2.00
stars: 
ratings: 
reviews: 
size: 
website: http://www.secuxtech.com
repository: 
issue: 
icon: com.secuxtech.secuxtwallet.png
bugbounty: 
meta: ok
verdict: fewusers
date: 2022-11-24
signer: 
reviewArchive: 
twitter: SecuXwallet
social: 
redirect_from: 

---

This is the companion app to the {% include walletLink.html wallet='hardware/secuxnifty' verdict='true' %}