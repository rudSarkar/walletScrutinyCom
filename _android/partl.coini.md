---
wsId: Coini
title: Coini - Crypto portfolio
altTitle: 
authors:
- danny
users: 10000
appId: partl.coini
appCountry: 
released: 2018-02-04
updated: 2022-12-16
version: 2.4.19
stars: 4.5
ratings: 291
reviews: 36
size: 
website: https://timopartl.com/Coini
repository: 
issue: 
icon: partl.coini.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2020-12-14
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from:
- /partl.coini/

---

This app is for portfolio tracking but probably is not in control of private keys.
