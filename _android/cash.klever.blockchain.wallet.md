---
wsId: klever
title: 'Klever: Secure Crypto Wallet'
altTitle: 
authors:
- leo
users: 1000000
appId: cash.klever.blockchain.wallet
appCountry: 
released: 2020-08-27
updated: 2023-01-13
version: 4.24.11
stars: 3.9
ratings: 10676
reviews: 583
size: 
website: https://www.klever.io/
repository: 
issue: 
icon: cash.klever.blockchain.wallet.png
bugbounty: 
meta: ok
verdict: nosource
date: 2021-05-22
signer: 
reviewArchive: 
twitter: klever_io
social:
- https://www.facebook.com/klever.io
redirect_from:
- /cash.klever.blockchain.wallet/

---

On their website we read:

> **Peer-to-Peer**<br>
  Klever is a decentralized p2p and self-custody wallet network. Your Keys, your
  crypto.

so they claim the app is self-custodial but we cannot find any source code which
makes the app **not verifiable**.
