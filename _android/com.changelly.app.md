---
wsId: 
title: 'Crypto Exchange: Buy Bitcoin'
altTitle: 
authors:
- leo
- danny
users: 100000
appId: com.changelly.app
appCountry: 
released: 2018-08-28
updated: 2022-12-29
version: 3.1.1
stars: 4.9
ratings: 5008
reviews: 1221
size: 
website: https://changelly.com/
repository: 
issue: 
icon: com.changelly.app.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-01
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from:
- /com.changelly.app/
- /posts/com.changelly.app/

---

This app has no wallet feature in the sense that you hold Bitcoins in the app.
