---
wsId: coinStore
title: Coinstore:Trade Crypto&Futures
altTitle: 
authors:
- danny
appId: com.coinstore.sg
appCountry: us
idd: '1567160644'
released: '2021-05-12T07:00:00Z'
updated: 2023-01-13
version: 1.4.8
stars: 3.9
reviews: 21
size: '128585728'
website: https://www.coinstore.com
repository: 
issue: 
icon: com.coinstore.sg.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-06-24
signer: 
reviewArchive: 
twitter: coinstore_en
social:
- https://www.linkedin.com/company/coinstore
- https://coinstore.medium.com
- https://www.facebook.com/coinstoreglobal
- https://t.me/coinstore_english

---

{% include copyFromAndroid.html %}