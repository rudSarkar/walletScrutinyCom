---
wsId: OGPay
title: OGPay Wallet
altTitle: 
authors:
- danny
appId: com.unifiedsignal.ogpaywallet
appCountry: us
idd: 1471960731
released: 2019-08-03
updated: 2023-01-17
version: '9.0'
stars: 4.8
reviews: 129
size: '189893632'
website: https://ogpaywallet.com/
repository: 
issue: 
icon: com.unifiedsignal.ogpaywallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-15
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
