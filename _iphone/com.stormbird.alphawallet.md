---
wsId: AlphaWallet
title: AlphaWallet Ethereum, Binance
altTitle: 
authors:
- danny
appId: com.stormbird.alphawallet
appCountry: us
idd: 1358230430
released: 2018-05-25
updated: 2023-01-20
version: '3.61'
stars: 4.7
reviews: 254
size: '106765312'
website: https://alphawallet.com/
repository: 
issue: 
icon: com.stormbird.alphawallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-10-10
signer: 
reviewArchive: 
twitter: Alpha_wallet
social:
- https://www.reddit.com/r/AlphaWallet
- https://github.com/alphawallet

---

{% include copyFromAndroid.html %}
