---
wsId: Baanx
title: Baanx App
altTitle: 
authors:
- danny
appId: com.baanx.baanxapp
appCountry: us
idd: 1437909986
released: 2018-12-20
updated: 2021-09-08
version: 3.1.0
stars: 3.6
reviews: 8
size: '53220352'
website: https://baanxapp.com
repository: 
issue: 
icon: com.baanx.baanxapp.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2022-12-28
signer: 
reviewArchive: 
twitter: baanx_bxx
social:
- https://www.facebook.com/baanxapp

---

{% include copyFromAndroid.html %}
