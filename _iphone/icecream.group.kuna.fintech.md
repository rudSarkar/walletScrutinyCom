---
wsId: kunaio
title: Kuna.io — buy sell crypto
altTitle: 
authors:
- danny
appId: icecream.group.kuna.fintech
appCountry: us
idd: 1457062155
released: 2019-03-27
updated: 2022-12-14
version: 4.3.4
stars: 4.5
reviews: 57
size: '172052480'
website: https://kuna.io
repository: 
issue: 
icon: icecream.group.kuna.fintech.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive: 
twitter: KunaExchange
social:
- https://www.facebook.com/kunaexchange

---

{% include copyFromAndroid.html %}
