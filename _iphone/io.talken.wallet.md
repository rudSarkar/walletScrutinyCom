---
wsId: Talken
title: Talken Multi-chain NFT Wallet
altTitle: 
authors:
- kiwilamb
appId: io.talken.wallet
appCountry: 
idd: 1459475831
released: 2019-09-25
updated: 2023-01-06
version: 1.01.11
stars: 5
reviews: 7
size: '139489280'
website: https://talken.io/
repository: 
issue: 
icon: io.talken.wallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-06-04
signer: 
reviewArchive: 
twitter: Talken_
social: 

---

{% include copyFromAndroid.html %}

