---
wsId: Coini
title: 'Coini: Crypto portfolio'
altTitle: 
authors:
- danny
appId: partl.coini
appCountry: us
idd: 1563051934
released: 2021-04-21
updated: 2022-12-10
version: 2.4.20
stars: 4.7
reviews: 13
size: '57849856'
website: https://timopartl.com
repository: 
issue: 
icon: partl.coini.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-10-29
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
