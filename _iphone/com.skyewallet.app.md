---
wsId: SkyeWallet
title: 'Skye Wallet: Buy & Sell Crypto'
altTitle: 
authors:
- danny
appId: com.skyewallet.app
appCountry: us
idd: 1587180582
released: 2021-09-27
updated: 2022-12-20
version: 2.12.5
stars: 3.3
reviews: 26
size: '54760448'
website: https://skyewallet.com/
repository: 
issue: 
icon: com.skyewallet.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-15
signer: 
reviewArchive: 
twitter: skyewallet
social: 

---

{% include copyFromAndroid.html %}
