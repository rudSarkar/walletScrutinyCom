---
wsId: BlockTrade
title: 'Blocktrade: Invest in Crypto'
altTitle: 
authors:
- danny
appId: com.blocktrade.ios
appCountry: us
idd: 1360294403
released: 2018-10-30
updated: 2022-12-08
version: 2.1.0
stars: 5
reviews: 9
size: '52377600'
website: http://blocktrade.com
repository: 
issue: 
icon: com.blocktrade.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-03
signer: 
reviewArchive: 
twitter: Blocktradecom
social:
- https://www.linkedin.com/company/blocktradecom
- https://www.facebook.com/Blocktradecom

---

{% include copyFromAndroid.html %}
