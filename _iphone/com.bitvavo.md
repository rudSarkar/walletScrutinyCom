---
wsId: bitvavo
title: Bitvavo | Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.bitvavo
appCountry: be
idd: 1483903423
released: 2020-05-28
updated: 2023-01-16
version: 2.11.1
stars: 4.6
reviews: 747
size: '39159808'
website: https://bitvavo.com
repository: 
issue: 
icon: com.bitvavo.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
