---
wsId: lyopay
title: LYOPAY
altTitle: 
authors:
- danny
appId: com.LYOPAY.LYOPAY
appCountry: eg
idd: 1537945402
released: 2020-11-06
updated: 2022-12-16
version: '8.13'
stars: 0
reviews: 0
size: '66282496'
website: https://lyopay.com/
repository: 
issue: 
icon: com.LYOPAY.LYOPAY.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: lyopayofficial
social:
- https://www.linkedin.com/company/lyopay
- https://www.facebook.com/lyopayofficial
- https://www.reddit.com/r/LYOPAY

---

{% include copyFromAndroid.html %}
