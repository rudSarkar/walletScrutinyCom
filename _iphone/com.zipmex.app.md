---
wsId: Zipmex
title: 'Zipmex: Buy Bitcoin & Crypto'
altTitle: 
authors:
- danny
appId: com.zipmex.app
appCountry: sg
idd: 1485647781
released: 2019-11-06
updated: 2022-10-12
version: 22.7.1
stars: 4.2
reviews: 125
size: '84703232'
website: https://www.youtube.com/watch?v=iYI01eFjxTg
repository: 
issue: 
icon: com.zipmex.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive: 
twitter: ZipmexTH
social:
- https://www.facebook.com/ZipmexThailand

---

{% include copyFromAndroid.html %}
