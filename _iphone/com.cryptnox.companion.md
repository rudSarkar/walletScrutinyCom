---
wsId: 
title: Cryptnox Wallet
altTitle: 
authors:
- danny
appId: com.cryptnox.companion
appCountry: us
idd: '1583011693'
released: '2021-10-10T07:00:00Z'
updated: 2022-12-19
version: 2.1.8
stars: 5
reviews: 1
size: '63045632'
website: https://www.cryptnox.com
repository: 
issue: 
icon: com.cryptnox.companion.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2022-11-26
signer: 
reviewArchive: 
twitter: 
social: 

---

This is the companion app to the {% include walletLink.html wallet='hardware/cryptnox.bg1card' verdict='true' %} hardware wallet.