---
wsId: Savl
title: 'Savl Wallet: Web3 & Crypto'
altTitle: 
authors:
- danny
appId: com.savl.savlapp
appCountry: ru
idd: 1369912925
released: 2018-04-22
updated: 2022-12-29
version: 4.0.0
stars: 4.4
reviews: 264
size: '222860288'
website: https://savl.com
repository: 
issue: 
icon: com.savl.savlapp.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-09-11
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/savl.official

---

{% include copyFromAndroid.html %}
