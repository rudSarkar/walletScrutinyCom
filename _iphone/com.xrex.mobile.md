---
wsId: XRex
title: XREX — Crypto swap to USD
altTitle: 
authors:
- danny
appId: com.xrex.mobile
appCountry: au
idd: 1482096895
released: 2020-02-19
updated: 2023-01-12
version: 2.0.468
stars: 0
reviews: 0
size: '66693120'
website: https://xrex.io/
repository: 
issue: 
icon: com.xrex.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-15
signer: 
reviewArchive: 
twitter: xrexinc
social:
- https://www.linkedin.com/company/xrexinc
- https://www.facebook.com/xrexinfo

---

{% include copyFromAndroid.html %}
