---
wsId: bingbon
title: 'BingX : Buy BTC & Crypto'
altTitle: 
authors:
- kiwilamb
- leo
appId: pro.bingbon.finance
appCountry: de
idd: 1500217666
released: 2020-02-25
updated: 2022-11-22
version: 3.23.0
stars: 3.9
reviews: 9
size: '133511168'
website: https://bingbon.com
repository: 
issue: 
icon: pro.bingbon.finance.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-04-24
signer: 
reviewArchive: 
twitter: BingbonOfficial
social:
- https://www.linkedin.com/company/bingbon
- https://www.facebook.com/BingbonOfficial
- https://www.reddit.com/r/Bingbon

---

We cannot find any claims as to the custody of private keys found from Bingbon.
We must assume the wallet app is custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

