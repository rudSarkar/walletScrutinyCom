---
wsId: korbit
title: 코빗 - 대한민국 최초, 가상자산 거래소
altTitle: 
authors:
- danny
appId: com.korbit.exchange
appCountry: us
idd: 1434511619
released: 2018-10-18
updated: 2023-01-19
version: 6.3.5
stars: 2.8
reviews: 16
size: '183446528'
website: http://www.korbit.co.kr
repository: 
issue: 
icon: com.korbit.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
