---
wsId: ObiexExchange
title: Obiex - Swap, Buy & Sell BTC
altTitle: 
authors:
- danny
appId: africa.obiex.app
appCountry: ng
idd: '1567887163'
released: '2021-06-18T07:00:00Z'
updated: 2023-01-03
version: 3.2.8
stars: 4.7
reviews: 374
size: '72010752'
website: 
repository: 
issue: 
icon: africa.obiex.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-08-01
signer: 
reviewArchive: 
twitter: obiexfinance
social:
- https://www.facebook.com/obiexfinance
- https://www.instagram.com/obiexfinance/

---

{% include copyFromAndroid.html %}