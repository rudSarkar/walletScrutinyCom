---
wsId: TaurosExchange
title: Tauros
altTitle: 
authors:
- danny
appId: com.taurosapp
appCountry: mx
idd: '1480919460'
released: '2019-12-08T08:00:00Z'
updated: 2022-12-31
version: '9.5'
stars: 3.5
reviews: 185
size: '88692736'
website: 
repository: 
issue: 
icon: com.taurosapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-07-31
signer: 
reviewArchive: 
twitter: tauros_io
social:
- https://www.instagram.com/tauros.io/
- https://www.facebook.com/tauros.io
- https://www.linkedin.com/company/taurosio/

---

{% include copyFromAndroid.html %}