---
wsId: changeinvest
title: 'Change: Buy Bitcoin & Ethereum'
altTitle: 
authors:
- danny
appId: com.getchange.dev
appCountry: nl
idd: 1442085358
released: 2018-11-15
updated: 2023-01-20
version: 20.36.1
stars: 4.1
reviews: 33
size: '119709696'
website: https://www.changeinvest.com/
repository: 
issue: 
icon: com.getchange.dev.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-01-04
signer: 
reviewArchive: 
twitter: changefinance
social:
- https://www.linkedin.com/company/changeinvest
- https://www.facebook.com/changeinvest

---

{% include copyFromAndroid.html %}
