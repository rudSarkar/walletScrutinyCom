---
wsId: foldapp
title: 'Fold: Bitcoin Cashback Rewards'
altTitle: 
authors:
- danny
appId: com.foldapp
appCountry: us
idd: 1480424785
released: 2019-11-18
updated: 2023-01-14
version: 139.18.2
stars: 4.4
reviews: 1400
size: '41901056'
website: http://foldapp.com
repository: 
issue: 
icon: com.foldapp.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-09-15
signer: 
reviewArchive: 
twitter: fold_app
social: 

---

{% include copyFromAndroid.html %}
