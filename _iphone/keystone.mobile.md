---
wsId: Keystone
title: Keystone Hardware Wallet
altTitle: 
authors:
- danny
appId: keystone.mobile
appCountry: 
idd: 1567857965
released: 2021-06-03
updated: 2023-01-18
version: 1.2.4
stars: 3.3
reviews: 16
size: '43811840'
website: https://keyst.one/
repository: 
issue: 
icon: keystone.mobile.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-17
signer: 
reviewArchive: 
twitter: KeystoneWallet
social:
- https://github.com/KeystoneHQ

---

{% include copyFromAndroid.html %}
