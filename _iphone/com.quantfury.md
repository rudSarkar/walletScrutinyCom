---
wsId: quantfury
title: 'Quantfury: Trading Made Honest'
altTitle: 
authors:
- danny
appId: com.quantfury
appCountry: gb
idd: 1445564443
released: 2018-12-15
updated: 2023-01-20
version: 1.55.0
stars: 4.5
reviews: 60
size: '59051008'
website: https://quantfury.com/
repository: 
issue: 
icon: com.quantfury.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-01
signer: 
reviewArchive: 
twitter: quantfury
social: 

---

{% include copyFromAndroid.html %}
