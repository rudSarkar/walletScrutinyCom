---
wsId: xverse
title: Xverse - Bitcoin Web3 Wallet
altTitle: 
authors:
- danny
appId: com.secretkeylabs.xverse
appCountry: gt
idd: 1552272513
released: 2021-10-15
updated: 2023-01-13
version: 1.10.0
stars: 0
reviews: 0
size: '46886912'
website: https://twitter.com/xverseApp
repository: 
issue: 
icon: com.secretkeylabs.xverse.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-13
signer: 
reviewArchive: 
twitter: secretkeylabs
social: 

---

{% include copyFromAndroid.html %}

