---
wsId: whitebit
title: WhiteBIT – buy & sell bitcoin
altTitle: 
authors:
- danny
appId: com.whitebit.whitebitapp
appCountry: ua
idd: 1463405025
released: 2019-05-21
updated: 2023-01-20
version: 2.23.2
stars: 4.4
reviews: 383
size: '257448960'
website: https://whitebit.com
repository: 
issue: 
icon: com.whitebit.whitebitapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive: 
twitter: whitebit
social:
- https://www.linkedin.com/company/whitebit-cryptocurrency-exchange
- https://www.facebook.com/whitebit
- https://www.reddit.com/r/WhiteBitExchange

---

{% include copyFromAndroid.html %}