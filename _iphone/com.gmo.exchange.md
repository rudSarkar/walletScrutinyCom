---
wsId: ZcomEx
title: Z.com EX - Buy/Sell Bitcoin
altTitle: 
authors:
- danny
appId: com.gmo.exchange
appCountry: th
idd: 1525862502
released: 2020-08-09
updated: 2023-01-17
version: 2.0.5
stars: 3.7
reviews: 25
size: '77768704'
website: https://ex.z.com/
repository: 
issue: 
icon: com.gmo.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-08
signer: 
reviewArchive: 
twitter: ZcomExchange
social:
- https://www.facebook.com/ZcomCrypto

---

{% include copyFromAndroid.html %}
