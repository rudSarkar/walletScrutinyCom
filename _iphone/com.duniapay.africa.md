---
wsId: DuniaPay
title: DuniaPay
altTitle: 
authors:
- danny
appId: com.duniapay.africa
appCountry: bf
idd: '1474570971'
released: '2019-10-18T07:00:00Z'
updated: 2022-04-10
version: 3.0.0
stars: 4.8
reviews: 11
size: '74934272'
website: https://www.duniapay.net
repository: 
issue: 
icon: com.duniapay.africa.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-08-01
signer: 
reviewArchive: 
twitter: dunia_pay
social:
- https://www.linkedin.com/company/dunia-payment/
- https://www.facebook.com/TeamDunia/
- https://www.youtube.com/channel/UCPJXe02QbAg3906lfr4pXzw

---

{% include copyFromAndroid.html %}