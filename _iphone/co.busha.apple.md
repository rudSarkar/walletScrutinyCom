---
wsId: busha
title: 'Busha: Trade BTC, ETH, SHIB'
altTitle: 
authors:
- leo
appId: co.busha.apple
appCountry: 
idd: 1450373493
released: 2019-02-03
updated: 2023-01-05
version: 5.4.0
stars: 4
reviews: 311
size: '102956032'
website: https://busha.co
repository: 
issue: 
icon: co.busha.apple.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive: 
twitter: getbusha
social:
- https://www.facebook.com/getbusha

---

The description

> Won’t you rather trade and store your crypto assets on a platform you can
  trust? Busha is a Nigerian based crypto exchange that offers you all these and
  more.

sounds like it's an app to access an account on a custodial platform.

On their website they are more explicit:

> **Safe & Secure**<br>
  Our 24/7 monitoring systems, cold storage and industry-standard multi-sig
  wallets ensure that your assets are the safest they can be.

which is a list of features only relevant in a custodial context.

Our verdict: **not verifiable**.
