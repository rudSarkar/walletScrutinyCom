---
wsId: BitPreco
title: Bitypreço
altTitle: 
authors:
- danny
appId: com.bitpreco.bitprecoApp
appCountry: br
idd: 1545825554
released: 2021-03-18
updated: 2022-12-05
version: 2.0.03
stars: 4.8
reviews: 620
size: '74467328'
website: https://bitpreco.com/
repository: 
issue: 
icon: com.bitpreco.bitprecoApp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-16
signer: 
reviewArchive: 
twitter: BitPreco
social:
- https://www.linkedin.com/company/bitpreco
- https://www.facebook.com/BitPreco

---

{% include copyFromAndroid.html %}

