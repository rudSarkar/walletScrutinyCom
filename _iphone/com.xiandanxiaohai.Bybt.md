---
wsId: 
title: Coinglass - BTC Futures Market
altTitle: 
authors:
- danny
appId: com.xiandanxiaohai.Bybt
appCountry: us
idd: 1522250001
released: 2020-07-08
updated: 2023-01-16
version: 1.5.8
stars: 4.9
reviews: 633
size: '20481024'
website: https://www.coinglass.com
repository: 
issue: 
icon: com.xiandanxiaohai.Bybt.jpg
bugbounty: 
meta: ok
verdict: fake
date: 2021-11-02
signer: 
reviewArchive: 
twitter: coinglass_com
social: 

---

{% include copyFromAndroid.html %}
