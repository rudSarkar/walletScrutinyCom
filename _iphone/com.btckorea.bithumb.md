---
wsId: bithumbko
title: Bithumb
altTitle: 
authors:
- leo
appId: com.btckorea.bithumb
appCountry: 
idd: 1299421592
released: 2017-12-05
updated: 2022-12-26
version: 1.8.8
stars: 1.9
reviews: 33
size: '139762688'
website: https://en.bithumb.com
repository: 
issue: 
icon: com.btckorea.bithumb.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive: 
twitter: BithumbOfficial
social:
- https://www.facebook.com/bithumb

---

This app is an interface to an exchange and to our knowledge only features
custodial accounts and therefore is **not verifiable**.
