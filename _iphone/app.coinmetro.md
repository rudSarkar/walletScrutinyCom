---
wsId: coinmetro
title: 'Coinmetro: Crypto Exchange'
altTitle: 
authors:
- danny
appId: app.coinmetro
appCountry: us
idd: 1397585225
released: 2018-07-25
updated: 2023-01-09
version: 5.4.513
stars: 4.2
reviews: 196
size: '33844224'
website: https://coinmetro.com/
repository: 
issue: 
icon: app.coinmetro.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive: 
twitter: CoinMetro
social:
- https://www.linkedin.com/company/coinmetro
- https://www.facebook.com/CoinMetro

---

{% include copyFromAndroid.html %}
