---
wsId: bitfy
title: Bitfy
altTitle: 
authors:
- danny
appId: app.bitfy
appCountry: us
idd: 1483269793
released: 2019-11-26
updated: 2023-01-20
version: 3.12.13
stars: 3
reviews: 4
size: '46559232'
website: https://bitfy.app
repository: 
issue: 
icon: app.bitfy.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-09-03
signer: 
reviewArchive: 
twitter: bitfyapp
social:
- https://www.facebook.com/bitfyapp

---

{% include copyFromAndroid.html %}
