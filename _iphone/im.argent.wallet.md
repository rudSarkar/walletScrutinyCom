---
wsId: argent
title: Argent – DeFi in a tap
altTitle: 
authors:
- danny
appId: im.argent.wallet
appCountry: us
idd: 1358741926
released: 2018-10-25
updated: 2022-12-24
version: 4.12.1
stars: 4.6
reviews: 1891
size: '156505088'
website: https://www.argent.xyz
repository: 
issue: 
icon: im.argent.wallet.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-01-10
signer: 
reviewArchive: 
twitter: argentHQ
social: 

---

{% include copyFromAndroid.html %}