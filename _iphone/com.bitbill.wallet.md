---
wsId: ownbit
title: 'Ownbit: Cold & MultiSig Wallet'
altTitle: 
authors:
- leo
appId: com.bitbill.wallet
appCountry: 
idd: 1321798216
released: 2018-02-07
updated: 2022-12-15
version: 4.39.0
stars: 4.4
reviews: 52
size: '124878848'
website: http://www.bitbill.com
repository: 
issue: 
icon: com.bitbill.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
