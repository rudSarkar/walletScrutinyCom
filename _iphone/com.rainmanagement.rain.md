---
wsId: rainfinancial
title: 'Rain: Buy & Sell Bitcoin'
altTitle: 
authors:
- danny
appId: com.rainmanagement.rain
appCountry: bh
idd: 1414619890
released: 2018-09-02
updated: 2022-12-22
version: 3.2.3
stars: 4.7
reviews: 2422
size: '81378304'
website: https://www.rain.bh/
repository: 
issue: 
icon: com.rainmanagement.rain.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: rainfinancial
social:
- https://www.linkedin.com/company/rainfinancial
- https://www.facebook.com/rainfinancial

---

 {% include copyFromAndroid.html %}
