---
wsId: bridgeWallet
title: Bridge Wallet
altTitle: 
authors:
- danny
appId: com.mtpelerin.bridge
appCountry: us
idd: 1481859680
released: 2020-04-08
updated: 2022-11-30
version: '1.27'
stars: 4.4
reviews: 40
size: '136537088'
website: https://www.mtpelerin.com/bridge-wallet
repository: 
issue: 
icon: com.mtpelerin.bridge.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-10
signer: 
reviewArchive: 
twitter: mtpelerin
social:
- https://www.linkedin.com/company/mt-pelerin
- https://www.facebook.com/mtpelerin
- https://www.reddit.com/r/MtPelerin

---

{% include copyFromAndroid.html %}
