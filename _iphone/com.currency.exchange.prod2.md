---
wsId: 
title: Crypto Exchange Currency.com
altTitle: 
authors:
- danny
appId: com.currency.exchange.prod2
appCountry: by
idd: 1458917114
released: 2019-04-23
updated: 2023-01-13
version: 1.35.0
stars: 4.8
reviews: 3301
size: '95840256'
website: https://currency.com/
repository: 
issue: 
icon: com.currency.exchange.prod2.jpg
bugbounty: 
meta: ok
verdict: wip
date: 2022-01-09
signer: 
reviewArchive: 
twitter: currencycom
social:
- https://www.facebook.com/currencycom
- https://www.reddit.com/r/currencycom

---

<!--
  According to the Android review, this app was falsely marked as wsId currencycominvesting and needs another close look.
-->
