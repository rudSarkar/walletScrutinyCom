---
wsId: ZuPago
title: ZuPago
altTitle: 
authors:
- danny
appId: app.zupago.zp
appCountry: us
idd: 1565673730
released: 2021-05-10
updated: 2022-10-26
version: 1.0.52
stars: 4.6
reviews: 53
size: '71836672'
website: https://zupago.app
repository: 
issue: 
icon: app.zupago.zp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-12-19
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}