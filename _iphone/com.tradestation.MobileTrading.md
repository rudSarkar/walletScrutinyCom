---
wsId: TradeStation
title: TradeStation - Trade & Invest
altTitle: 
authors:
- danny
appId: com.tradestation.MobileTrading
appCountry: us
idd: 581548081
released: 2012-12-10
updated: 2023-01-03
version: 6.1.6
stars: 4.5
reviews: 16655
size: '42589184'
website: http://www.tradestation.com/trading-technology/tradestation-mobile
repository: 
issue: 
icon: com.tradestation.MobileTrading.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive: 
twitter: tradestation
social:
- https://www.facebook.com/TradeStation

---

{% include copyFromAndroid.html %}
