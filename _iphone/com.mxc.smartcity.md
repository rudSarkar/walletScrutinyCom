---
wsId: datadash
title: DataDash App
altTitle: 
authors:
- danny
appId: com.mxc.smartcity
appCountry: us
idd: 1509218470
released: 2020-06-30
updated: 2023-01-16
version: 5.0.3
stars: 4
reviews: 103
size: '82768896'
website: http://mxc.org
repository: 
issue: 
icon: com.mxc.smartcity.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2021-10-01
signer: 
reviewArchive: 
twitter: mxcfoundation
social:
- https://www.facebook.com/MXCfoundation
- https://www.reddit.com/r/MXC_Foundation

---

{% include copyFromAndroid.html %}
