---
wsId: sparkPoint
title: SparkPoint Crypto Wallet
altTitle: 
authors:
- danny
appId: io.sparkpoint.ios.wallet
appCountry: us
idd: 1572629350
released: 2021-06-26
updated: 2022-12-16
version: 11.1.0
stars: 4.8
reviews: 16
size: '24768512'
website: https://sparkpoint.io/
repository: 
issue: 
icon: io.sparkpoint.ios.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2022-01-10
signer: 
reviewArchive: 
twitter: sparkpointio
social:
- https://www.linkedin.com/company/sparkpointio
- https://www.facebook.com/sparkpointio
- https://www.reddit.com/r/SparkPoint

---

{% include copyFromAndroid.html %}