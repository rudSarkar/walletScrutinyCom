---
wsId: bitberry
title: 'Bitberry : Safe Wallet'
altTitle: 
authors:
- danny
appId: com.rootone.bitberry
appCountry: us
idd: 1411817291
released: 2018-10-09
updated: 2022-12-28
version: 1.4.0
stars: 4.1
reviews: 7
size: '89567232'
website: http://bitberry.app
repository: 
issue: 
icon: com.rootone.bitberry.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-02-22
signer: 
reviewArchive: 
twitter: 
social: 

---

{% include copyFromAndroid.html %}
