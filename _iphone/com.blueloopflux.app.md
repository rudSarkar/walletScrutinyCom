---
wsId: FluxPay
title: Flux Pay
altTitle: 
authors:
- danny
appId: com.blueloopflux.app
appCountry: us
idd: 1534426282
released: 2020-10-15
updated: 2022-12-11
version: 3.0.20
stars: 3.8
reviews: 66
size: '67110912'
website: https://iflux.app/
repository: 
issue: 
icon: com.blueloopflux.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-24
signer: 
reviewArchive: 
twitter: ifluxdotapp
social:
- https://www.linkedin.com/company/iflux-pay

---

{% include copyFromAndroid.html %}
